import os
import base64
import time
from random import randint

from Crypto import Random
from Crypto.Cipher import AES

from peer_socket import PeerSocket

BUFFERN = 32
PADDING = '{'


def _unpad(s):
    return s[:-ord(s[len(s) - 1:])]


def _pad(s):
    return s + ((BUFFERN - len(s) % BUFFERN) * chr(BUFFERN - len(s) % BUFFERN))


def encrypt(key, raw):
    raw = _pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw.encode()))


def decrypt(key, enc):
    enc = base64.b64decode(enc)
    iv = enc[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    try:
        return _unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
    except UnicodeDecodeError:
        return "Error data"


def greeting_wrapper(key):
    def hello(sender_addr, message):
        decrypted = decrypt(key, message)
        if decrypted == "Current primary node is traitor":
            print(str(sender_addr) + ' said ' + decrypted, " (raw: ", message, " )")
            return "OK"
        else:
            return main_node

    return hello


good_key = os.urandom(BUFFERN)
wrong_key = os.urandom(BUFFERN)
votes_received = dict()

peers = [
    PeerSocket(('localhost', 6001), good_key),
    PeerSocket(('localhost', 7001), good_key),
    PeerSocket(('localhost', 8001), good_key),
    PeerSocket(('localhost', 9001), wrong_key),
    PeerSocket(('localhost', 9101), wrong_key)
]
main_node = randint(0, 4)

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


def response(message):
    global votes_received
    if str(message) == str(main_node):
        if main_node not in votes_received:
            votes_received[main_node] = 1
        else:
            votes_received[main_node] += 1

        if votes_received[main_node] > 2:
            print(color.BOLD + color.GREEN + "{} is now the main node".format(main_node) + color.END)
            for key in votes_received.keys():
                if key != main_node:
                    votes_received[key] = 0
    print('Received text is ' + str(message))


event = 'HELLO'

for x in peers:
    x.on(event, greeting_wrapper(x.key))

while True:
    main_node = randint(0, 4)
    for x in peers:
        peers[main_node].send(x.addr, event, encrypt(peers[main_node].key, "Current primary node is traitor"), response)
    time.sleep(3)
    print(color.BOLD + color.BLUE + 'Current votes: {}'.format(votes_received) + color.END)
    continue